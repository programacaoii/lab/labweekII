# Laboratório - Semana II: Programação

Bem-vindo ao laboratório da Semana II de programaçãoII!.

## Atividades

Aqui estão os links para acessar os arquivos de cada atividade:

- <a href="https://gitlab.com/programacaoii/lab/labweekII/-/tree/main/src/semanaII/atvI" target="_blank">Atividade 1</a>
- <a href="https://gitlab.com/programacaoii/lab/labweekII/-/tree/main/src/semanaII/atvII" target="_blank">Atividade 2</a>
- <a href="https://gitlab.com/programacaoii/lab/labweekII/-/tree/main/src/semanaII/atvIII" target="_blank">Atividade 3</a>
- <a href="https://gitlab.com/programacaoii/lab/labweekII/-/tree/main/src/semanaII/atvIV" target="_blank">Atividade 4</a>

Clique nos links acima para abrir cada arquivo em uma nova aba do seu navegador.



