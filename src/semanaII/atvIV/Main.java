package semanaII.atvIV;

public class Main {

	public static void main(String[] args) {
		AnimalEstimacao m1 = new Gato();
		AnimalEstimacao m2 = new Pato();
		AnimalEstimacao m3 = new Lagarto();
		AnimalEstimacao m4 = new Falcao();
		AnimalEstimacao m5 = new Morcego();
		
		Jogador jogador = new Jogador();
		jogador.adquirirMascote(m1);
		jogador.adquirirMascote(m2);

		jogador.adquirirMascote(m3);
		jogador.adquirirMascote(m4);
		jogador.adquirirMascote(m5);
		jogador.listarComportamentosMascotes();
		
		System.out.print("O jogador tem animais de estimacao: ");
		jogador.exibirMascotes();
	}

}
