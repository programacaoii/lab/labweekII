package semanaII.atvIV;

public class Lagarto extends AnimalEstimacao {

	public Lagarto() {
		super("Lagarto");

	}

	@Override
	public void comer() {
		System.out.println("O lagarto está comendo.");
	}

	@Override
	public void dormir() {
		System.out.println("O lagarto está dormindo.");
	}

	@Override
	public void treinar() {
		System.out.println("O lagarto está sendo treinado.");
	}
}
