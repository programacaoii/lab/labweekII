package semanaII.atvIV;

public class Gato extends AnimalEstimacao {

	public Gato() {
		super("Gato");

	}

	@Override
	public void comer() {
		System.out.println("O gato esta comendo.");

	}

	@Override
	public void dormir() {
		System.out.println("O gato esta dormindo.");

	}

	@Override
	public void treinar() {
		System.out.println("O gato esta sendo treinado.");

	}

}
