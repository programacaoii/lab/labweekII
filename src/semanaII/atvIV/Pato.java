package semanaII.atvIV;

public class Pato extends AnimalEstimacao {

	public Pato() {
		super("pato");
	}

	@Override
	public void comer() {
		System.out.println("O pato está comendo.");
	}

	@Override
	public void dormir() {
		System.out.println("O pato está dormindo.");
	}

	@Override
	public void treinar() {
		System.out.println("O pato está sendo treinado.");
	}
}