package semanaII.atvIV;

public class Falcao extends AnimalEstimacao {
    public Falcao() {
    	super("Falcao");
	}

	@Override
    public void comer() {
        System.out.println("O falcão está comendo.");
    }

    @Override
    public void dormir() {
        System.out.println("O falcão está dormindo.");
    }

    @Override
    public void treinar() {
        System.out.println("O falcão está sendo treinado.");
    }
}
