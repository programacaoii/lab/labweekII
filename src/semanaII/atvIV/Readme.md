# Meu Animal de Estimação no Videogame

A nova versão do nosso videogame permitirá que os jogadores treinem um animal de estimação para colaborar em jogos de RPG ou RMPG. O animal de estimação poderá ser adquirido após a conclusão do treinamento básico de zoologia e será muito útil para explorar terrenos desconhecidos, carregar itens ou entregar mensagens.

## Descrição da Atividade

A atividade consiste em criar uma estrutura que permita criar um animal de estimação de diferentes tipos (pássaro, mamífero, réptil) e adicionar comportamentos específicos de acordo com o tipo de animal observado na natureza.

A base será uma classe abstrata que representa as características básicas comuns a qualquer animal e comportamentos comuns a qualquer animal, como comer, dormir e treinar. A implementação inicial incluirá animais de estimação de três tipos: pássaro, mamífero e réptil.

Os comportamentos que um animal de estimação pode aprender para se movimentar no terreno são: andar, voar e nadar.

## Funcionalidades

O projeto "Meu Animal de Estimação no Videogame" oferece as seguintes funcionalidades:

- Criação de um animal de estimação de qualquer tipo: pássaro, mamífero, réptil.
- Adição de comportamentos específicos ao animal de estimação com base nos tipos de animais planejados (exemplo: gato, pato, morcego, falcão, lagarto).
- Comportamentos básicos do animal de estimação, como comer, dormir e treinar.
- Comportamentos de movimentação no terreno: andar, voar, nadar.


## Diagrama
<img src="../img/packageatv4.png" width="60%">

## Execução
<img src="../img/imgatv4.png" width="80%">
