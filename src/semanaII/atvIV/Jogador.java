package semanaII.atvIV;

import java.util.ArrayList;
import java.util.List;

public class Jogador {
	private List<AnimalEstimacao> animais;

	public Jogador() {
		animais = new ArrayList<>();
	}

	public void adquirirMascote(AnimalEstimacao animal) {
		animais.add(animal);
	}

	public void listarComportamentosMascotes() {
		for (AnimalEstimacao animal : animais) {
			if (animal instanceof Gato) {
				System.out.println("Comportamento do Gato: pode andar");
			} else if (animal instanceof Pato) {
				System.out.println("Comportamento do Pato: pode andar, pode voar, pode nadar");
			} else if (animal instanceof Lagarto) {
				System.out.println("Comportamento do Lagarto: consegue andar, consegue voar");
			} else if (animal instanceof Falcao) {
				System.out.println("Comportamento do Falcão: consegue voar, consegue andar");
			} else if (animal instanceof Morcego) {
				System.out.println("Comportamento do Morcego: pode voar");
			}
		}
	}

	public void exibirMascotes() {

		for (int i = 0; i < animais.size(); i++) {
			if (i > 0) {
				System.out.print(", ");
			}
			System.out.print("m" + (i + 1) + ": " + animais.get(i).getNome()+"");
		}
		System.out.println();

	}

}
