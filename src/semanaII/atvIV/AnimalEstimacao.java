package semanaII.atvIV;

public abstract class AnimalEstimacao {

	private String nome;

	public AnimalEstimacao(String nome) {
		this.nome = nome;
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public abstract void comer();

	public abstract void dormir();

	public abstract void treinar();

}
