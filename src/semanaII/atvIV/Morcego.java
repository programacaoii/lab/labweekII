package semanaII.atvIV;

public class Morcego extends AnimalEstimacao {
	public Morcego() {
		super("Morcego");

	}

	@Override
	public void comer() {
		System.out.println("O morcego está comendo.");
	}

	@Override
	public void dormir() {
		System.out.println("O morcego está dormindo.");
	}

	@Override
	public void treinar() {
		System.out.println("O morcego está sendo treinado.");
	}
}
