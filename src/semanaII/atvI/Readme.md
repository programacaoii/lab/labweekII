# MySet - Implementação de conjunto não classificada

Este é um programa em Java que implementa uma lista de inteiros não classificada chamada "MySet". Ele permite ao usuário adicionar novos números inteiros à lista e lança exceções personalizadas em determinadas condições.

## Funcionalidades

- Adicionar um novo número inteiro à lista
- Verificar se um valor já existe na lista antes de adicioná-lo
- Lançar exceções personalizadas em várias situações, como repetição de valores, valores nulos ou muito grandes, e limite máximo de elementos na lista

## Diagrama
<img src="../img/packageatv1.png" width="50%">

## Execução
<img src="../img/imgatv1.png" width="50%">