package semanaII.atvI;

public  class MySet {

	private final int[] array;
    private int size;

    public MySet() {
        this.array = new int[100];
        this.size = 0;
    }

    public void add(Short  value) throws ExceptionMenssage {
        if (value==null) {
            throw new ExceptionMenssage("Valor nulo não é permitido.");
        }

        if (value >=Short.MAX_VALUE) {
            throw new IllegalArgumentException("Valor muito grande.");
        }
        for (int i = 0; i < size; i++) {
            if (array[i] == value) {
                throw new ExceptionMenssage("Este valor ja existe na lista.");
            }
        }

        if (size >= 100) {
            throw new ExceptionMenssage("A lista já esta cheia.");
        }

        array[size] = value;
        size++;
    }
}
