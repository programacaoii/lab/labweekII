package semanaII.atvI;

import java.util.Scanner;

public class Main {
	public static void main(String[] args) throws ExceptionMenssage {
		MySet set = new MySet();
		Scanner scanner = new Scanner(System.in);

		while (true) {
			System.out.print("Digite um numero positivo (ou um numero negativo para sair): ");
			Short number = scanner.nextShort();
			if (number < 0) {
				break;
			}
			try {
				set.add(number);
				System.out.println("Numero adicionado com sucesso!");
			} catch (ExceptionMenssage e) {
				System.out.println("Erro: " + e.getMessage());
			}

		}

	}
}
