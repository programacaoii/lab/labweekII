package semanaII.atvIII;

public class Flauta extends Instrumento {

	@Override
	public void play() {
		System.out.println("Tocando a Flauta");

	}

	@Override
	public String toString() {
		return "Flauta";
	}

}
