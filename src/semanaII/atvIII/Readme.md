# Minha Própria Orquestra

Neste projeto, vamos criar uma orquestra e, como primeira etapa, precisamos modelar os instrumentos musicais.

## Descrição da Atividade

A atividade consiste em criar uma classe abstrata chamada "Instrumento" com dois métodos abstratos: `play()` e `toString()`. Os instrumentos musicais serão classificados em três categorias básicas:

1. Instrumentos de corda: Por exemplo, violão, violino. Para produzir som, será utilizado o método `play()`.
2. Instrumentos de percussão: Por exemplo, tambor, pratos. Esses instrumentos produzem som constante e não exigem uma nota específica.
3. Instrumentos de sopro: Por exemplo, flauta, trompete. Para produzir som, é necessário fornecer uma nota como parâmetro.

## Funcionalidades

O projeto "Minha Própria Orquestra" oferece as seguintes funcionalidades:

- Modelagem de instrumentos musicais em três categorias básicas: instrumentos de corda, instrumentos de percussão e instrumentos de sopro.
- Classe abstrata `Instrumento` com métodos abstratos `play()` e `toString()` para definir a funcionalidade básica de um instrumento.
- Criação de subclasses da classe `Instrumento` para representar instrumentos específicos em cada categoria.
- Implementação personalizada do método `play()` em cada subclass para produzir o som do instrumento.
- Implementação personalizada do método `toString()` em cada subclass para fornecer uma representação em formato de string do instrumento.

## Diagrama
<img src="../img/packageatv3.png" width="80%">

## Execução
<img src="../img/imgatv3.png" width="30%">
