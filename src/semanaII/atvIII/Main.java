package semanaII.atvIII;

public class Main {

	public static void main(String[] args) {
		Instrumento violao = new Violao();
		Instrumento violino = new Violino();
		Instrumento tambor = new Tambor();
		Instrumento pratos = new Pratos();
		Instrumento flauta = new Flauta();
		Instrumento trompete = new Trompete();

		Orquestra orquestra = new Orquestra();
		orquestra.adicionarInstrumento(violao);
		orquestra.adicionarInstrumento(violino);
		orquestra.adicionarInstrumento(tambor);
		orquestra.adicionarInstrumento(pratos);
		orquestra.adicionarInstrumento(flauta);
		orquestra.adicionarInstrumento(trompete);

		orquestra.tocarSelecao(0);
		System.out.println("");
		System.out.println("Orquestra tocando:");
		orquestra.tocarSequencia();
	}

}
