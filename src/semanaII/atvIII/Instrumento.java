package semanaII.atvIII;

abstract class Instrumento {
	public abstract void play();

	public abstract String toString();
}
