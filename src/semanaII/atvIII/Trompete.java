package semanaII.atvIII;

public class Trompete extends Instrumento {

	@Override
	public void play() {
		System.out.println("Tocando os Trompete");

	}

	@Override
	public String toString() {
		return "Trompete";
	}

}
