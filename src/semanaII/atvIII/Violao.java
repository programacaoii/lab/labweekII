package semanaII.atvIII;

public class Violao extends Instrumento {

	@Override
	public void play() {
		System.out.println("Tocando o violao");

	}

	@Override
	public String toString() {
		return "Violao";
	}

}
