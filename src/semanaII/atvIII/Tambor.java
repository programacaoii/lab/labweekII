package semanaII.atvIII;

public class Tambor extends Instrumento {

	@Override
	public void play() {
		System.out.println("Tocando o Tambor");

	}

	@Override
	public String toString() {
		return "Tambor";
	}

}
