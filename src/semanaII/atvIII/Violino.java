package semanaII.atvIII;

public class Violino extends Instrumento {

	@Override
	public void play() {
		System.out.println("Tocando o Violino");

	}

	@Override
	public String toString() {
		return "Violino";
	}

}
