package semanaII.atvIII;

import java.util.ArrayList;
import java.util.List;

public class Orquestra {

	private List<Instrumento> instrumentos;

	public Orquestra() {
		this.instrumentos = new ArrayList<>();
	}

	public void adicionarInstrumento(Instrumento instrumento) {
		instrumentos.add(instrumento);
	}

	public void tocarSelecao(int indice) {
		if (indice >= 0 && indice < instrumentos.size()) {
			Instrumento instrumento = instrumentos.get(indice);
			instrumento.play();
		} else {
			System.out.println("Indice invalido!");
		}
	}

	public void tocarSequencia() {
		for (Instrumento instrumento : instrumentos) {
			instrumento.play();
		}
	}
}
