# Classificar Vários Arranjos Numéricos

Este é um projeto para classificar diferentes arranjos numéricos em Java.

## Funcionalidades

O projeto de classificação de arranjos numéricos oferece as seguintes funcionalidades:

- Criação de subclasses para gerenciar matrizes de diferentes tipos numéricos, como int[], float[], double[], byte[], short[], long[] e char[].
- Inserção de itens nos arranjos.
- Listagem dos itens presentes nos arranjos.
- Remoção de itens dos arranjos.
- Ordenação dos elementos em ordem decrescente utilizando o método "sort()".
- Listagem dos itens uma vez ordenados.


## Diagrama
<img src="../img/packageatv2.png" width="80%">

## Execução
<img src="../img/imgatv2.png" width="30%">

##

<img src="../img/imgatv2.1.png" width="30%">