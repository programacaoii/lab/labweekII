package semanaII.atvII;

public class Main {
    public static void main(String[] args) {
        // Exemplo com IntArranjo
        IntArranjo intArranjo = new IntArranjo(5);
        intArranjo.inserirItem(2);
        intArranjo.inserirItem(1);
        intArranjo.inserirItem(6);
        intArranjo.inserirItem(8);
        intArranjo.inserirItem(3);

        System.out.println("IntArranjo - Itens na matriz:");
        intArranjo.listarItens();

        System.out.println("\nIntArranjo - Matriz ordenada decrescentes:");
        intArranjo.sort();
        intArranjo.listarItens();
        
        System.out.println("\nIntArranjo - Itens apos a remocao:");
        intArranjo.removerItem(2);
        intArranjo.listarItens();

        System.out.println("\n--------------------------------------");

        // Exemplo com FloatArranjo
        FloatArranjo floatArranjo = new FloatArranjo(4);
        floatArranjo.inserirItem(0.2f);
        floatArranjo.inserirItem(0.5f);
        floatArranjo.inserirItem(1.2f);
        floatArranjo.inserirItem(2.5f);

        System.out.println("\n\nFloatArranjo - Itens na matriz:");
        floatArranjo.listarItens();

        System.out.println("\nFloatArranjo - Matriz ordenada decrescentes:");
        floatArranjo.sort();
        floatArranjo.listarItens();

        System.out.println("\nFloatArranjo - Itens apos a remocao:");
        floatArranjo.removerItem(2.2f);
        floatArranjo.listarItens();

        System.out.println("\n--------------------------------------");

        // Exemplo com DoubleArranjo
        DoubleArranjo doubleArranjo = new DoubleArranjo(4);
        doubleArranjo.inserirItem(0.2);
        doubleArranjo.inserirItem(0.5);
        doubleArranjo.inserirItem(1.2);
        doubleArranjo.inserirItem(2.5);

        System.out.println("\n\nDoubleArranjo - Itens na matriz:");
        doubleArranjo.listarItens();

       
        System.out.println("\nDoubleArranjo - Matriz ordenada decrescentes:");
        doubleArranjo.sort();
        doubleArranjo.listarItens();
        
        System.out.println("\nDoubleArranjo - Itens apos a remocao:");
        doubleArranjo.removerItem(2.2);
        doubleArranjo.listarItens();

        System.out.println("\n--------------------------------------");

        // Exemplo com ShortArranjo
        ShortArranjo shortArranjo = new ShortArranjo(5);
        shortArranjo.inserirItem((short) 5);
        shortArranjo.inserirItem((short) 2);
        shortArranjo.inserirItem((short) 8);
        shortArranjo.inserirItem((short) 1);
        shortArranjo.inserirItem((short) 9);

        System.out.println("\n\nShortArranjo - Itens na matriz:");
        shortArranjo.listarItens();

        System.out.println("\nShortArranjo - Matriz ordenada decrescentes:");
        shortArranjo.sort();
        shortArranjo.listarItens(); 
        
        System.out.println("\nShortArranjo - Itens apos a remocao:");
        shortArranjo.removerItem((short) 2);
        shortArranjo.listarItens();

        System.out.println("\n--------------------------------------");

        // Exemplo com LongArranjo
        LongArranjo longArranjo = new LongArranjo(5);
        longArranjo.inserirItem(5000000000L);
        longArranjo.inserirItem(2000000000L);
        longArranjo.inserirItem(8000000000L);
        longArranjo.inserirItem(1000000000L);
        longArranjo.inserirItem(9000000000L);

        System.out.println("\n\nLongArranjo - Itens na matriz:");
        longArranjo.listarItens();

        System.out.println("\nLongArranjo - Matriz ordenada decrescentes:");
        longArranjo.sort();
        longArranjo.listarItens(); 
        
        System.out.println("\nLongArranjo - Itens apos a remocao:");
        longArranjo.removerItem(2000000000L);
        longArranjo.listarItens();

        System.out.println("\n--------------------------------------");

        // Exemplo com ByteArranjo
        ByteArranjo byteArranjo = new ByteArranjo(2);
        byteArranjo.inserirItem((byte) 0);
        byteArranjo.inserirItem((byte) 1);


        System.out.println("\n\nByteArranjo - Itens na matriz:");
        byteArranjo.listarItens();

        System.out.println("\nByteArranjo - Matriz ordenada decrescentes:");
        byteArranjo.sort();
        byteArranjo.listarItens();

        System.out.println("\nByteArranjo - Itens apos a remocao:");
        byteArranjo.removerItem((byte) 1);
        byteArranjo.listarItens();

        System.out.println("\n--------------------------------------");

        // Exemplo com CharArranjo
        CharArranjo charArranjo = new CharArranjo(4);
        charArranjo.inserirItem('n');
        charArranjo.inserirItem('v');
        charArranjo.inserirItem('d');
        charArranjo.inserirItem('a');
     

        System.out.println("\n\nCharArranjo - Itens na matriz:");
        charArranjo.listarItens();

  
        System.out.println("\nCharArranjo - Matriz ordenada decrescentes:");
        charArranjo.sort();
        charArranjo.listarItens(); 
       

        System.out.println("\nCharArranjo - Itens apos a remocao:");
        charArranjo.removerItem('B');
        charArranjo.listarItens();
    }
}

