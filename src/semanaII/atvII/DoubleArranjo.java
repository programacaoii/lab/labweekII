package semanaII.atvII;

import java.util.Arrays;

public class DoubleArranjo extends Arranjo {
	private double[] numeros;
	private int tamanho;

	public DoubleArranjo(int tamanho) {
		this.numeros = new double[tamanho];
		this.tamanho = 0;
	}

	@Override
	public void sort() {
		Arrays.sort(numeros);
		int i = 0;
		int j = tamanho - 1;
		while (i < j) {
			double temp = numeros[i];
			numeros[i] = numeros[j];
			numeros[j] = temp;
			i++;
			j--;
		}
	}

	public void inserirItem(double item) {
		if (tamanho < numeros.length) {
			numeros[tamanho] = item;
			tamanho++;
		} else {
			System.out.println("A matriz esta cheia. Nao é possivel inserir mais itens.");
		}
	}

	public void listarItens() {
		for (int i = 0; i < tamanho; i++) {
			System.out.print("[ " + numeros[i] + " ] ");
		}
	}

	public void removerItem(double item) {
		int indice = -1;
		for (int i = 0; i < tamanho; i++) {
			if (numeros[i] == item) {
				indice = i;
				break;
			}
		}
		if (indice != -1) {
			for (int i = indice; i < tamanho - 1; i++) {
				numeros[i] = numeros[i + 1];
			}
			tamanho--;
			System.out.println("Item [ "+item+" ] removido com sucesso.");
        } else {
            System.out.println("Item [ "+item+" ] nao encontrado na matriz.");
		}

	}

}
