package semanaII.atvII;

import java.util.Arrays;

public class IntArranjo extends Arranjo {
	private int[] numeros;
	private int tamanho;

	public IntArranjo(int tamanho) {
		this.numeros = new int[tamanho];
		this.tamanho = 0;
	}

	@Override
	public void sort() {
		Arrays.sort(numeros);
		int i = 0;
		int j = numeros.length - 1;
		while (i < j) {
			int temp = numeros[i];
			numeros[i] = numeros[j];
			numeros[j] = temp;
			i++;
			j--;
		}
	}

	public void inserirItem(int item) {
		if (tamanho < numeros.length) {
			numeros[tamanho] = item;
			tamanho++;
		} else {
			System.out.println("A matriz está cheia. Nao é possível inserir mais itens.");
		}
	}

	public void listarItens() {
		for (int i = 0; i < tamanho; i++) {
			System.out.print("[ "+numeros[i]+" ] ");
		}
	}

	public void removerItem(int item) {
		int indice = -1;
		for (int i = 0; i < tamanho; i++) {
			if (numeros[i] == item) {
				indice = i;
				break;
			}
		}
		if (indice != -1) {
			for (int i = indice; i < tamanho - 1; i++) {
				numeros[i] = numeros[i + 1];
			}
			tamanho--;
			System.out.println("Item [ "+item+" ] removido com sucesso.");
        } else {
            System.out.println("Item [ "+item+" ] nao encontrado na matriz.");
		}
	}
}
