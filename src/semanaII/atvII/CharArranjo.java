package semanaII.atvII;

import java.util.Arrays;

public class CharArranjo extends Arranjo {
    private char[] caracteres;
    private int tamanho;

    public CharArranjo(int tamanho) {
        this.caracteres = new char[tamanho];
        this.tamanho = 0;
    }

    @Override
    public void sort() {
        Arrays.sort(caracteres);
        int i = 0;
        int j = tamanho - 1;
        while (i < j) {
            char temp = caracteres[i];
            caracteres[i] = caracteres[j];
            caracteres[j] = temp;
            i++;
            j--;
        }
    }

    public void inserirItem(char item) {
        if (tamanho < caracteres.length) {
            caracteres[tamanho] = item;
            tamanho++;
        } else {
            System.out.println("A matriz esta cheia. Nao e possivel inserir mais itens.");
        }
    }

    public void listarItens() {
        for (int i = 0; i < tamanho; i++) {
        	System.out.print("[ " + caracteres[i] + " ] ");
        }
    }

    public void removerItem(char item) {
        int indice = -1;
        for (int i = 0; i < tamanho; i++) {
            if (caracteres[i] == item) {
                indice = i;
                break;
            }
        }
        if (indice != -1) {
            for (int i = indice; i < tamanho - 1; i++) {
                caracteres[i] = caracteres[i + 1];
            }
            tamanho--;
            System.out.println("Item [ "+item+" ] removido com sucesso.");
        } else {
            System.out.println("Item [ "+item+" ] nao encontrado na matriz.");
		}
    }
 
}